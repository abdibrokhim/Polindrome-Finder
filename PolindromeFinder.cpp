
// Author --> LONE.Guy (Ibrohim Abdivokhidov)
// Ex-Student of INHA University, Currently Student of New Uzbekistan University
// Rich me --> Instagram: @loneguy | Github: @abdibrokhim
// !ALL RIGHTS RESERVED!

// Task --> Polindrome Finder

#include <iostream>

using namespace std;

int main() {
  std::string text, reversed_text;
  std::cin >> text;
  
  reversed_text = "";
  
  for (int i = text.size() - 1; i >= 0; i--) {
    reversed_text += text[i];
  }
  
  if (reversed_text == text) {
    std::cout << "true" << "\n";
  }
  
  else{
    std::cout << "false" << "\n";
  }
  
  
    return 0;
}

